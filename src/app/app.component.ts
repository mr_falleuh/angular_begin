import { Component } from '@angular/core';
import { Personne } from './classes/personne';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProjectName';
  // personne: Personne;
  // constructor() {
  //   this.personne = new Personne("Fall", "Monsieur");
  // }

  personne: Personne;
  tab: number[] = [2, 3, 5, 8];
  constructor() {
    this.personne = new Personne("Fall", "MAKHOU");
  }
  
  direBonjour() {
    return "bonjour Angular";
  }
}
